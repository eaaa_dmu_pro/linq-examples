﻿using Example01;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example05
{
    class Program
    {
        static void Main(string[] args)
        {
            var people = PeopleService.GetTestData();
            var res = people.OrderBy(p => p.Name).GroupBy(p => p.Rating, p => p.Name).OrderBy(g => g.Key);

            foreach (var group in res)
            {
                Console.WriteLine("Rating: " + group.Key);
                foreach (string item in group)
                    Console.WriteLine("  - " + item);
            }
            Console.WriteLine("--------------------------");

            var res2 = people.GroupBy(p => p.Rating, p => p);

            foreach (var group in res2)
            {
                Console.WriteLine("Rating: " + group.Key);
                foreach (Person item in group)
                    Console.WriteLine("  - " + item);
            }

            Console.ReadLine();

        }
    }
}
