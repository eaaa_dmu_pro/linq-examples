﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example04
{
    class Program
    {
        static void Main(string[] args)
        {
            var stores = TestDataService.GetStores();
            var addresses = TestDataService.GetAddresses();

            var res = from store in stores
                      join address in addresses
                      on store.Zip equals address.Zip
                      select new { StoreName = store.Name, StreetName = address.StreetName, StreenNumber = address.StreetNumber };
            foreach (var r in res)
            {
                Console.WriteLine($"{r.StoreName} er i nærheden af {r.StreetName} {r.StreenNumber}.");
            }

            Console.WriteLine("------------------------");
            Console.WriteLine();

            var res2 = from store2 in stores
                      from address2 in addresses
                      where store2.Zip == address2.Zip
                      select new { StoreName = store2.Name, StreetName = address2.StreetName, StreenNumber = address2.StreetNumber };

            foreach (var r in res2)
            {
                Console.WriteLine($"{r.StoreName} er i nærheden af {r.StreetName} {r.StreenNumber}.");
            }

            Console.WriteLine("------------------------");
            Console.WriteLine();

            var res3 = stores.Join(addresses, s => s.Zip, a => a.Zip, 
                (store, address) => 
                new
                {
                    StoreName = store.Name,
                    StreetName = address.StreetName,
                    StreenNumber = address.StreetNumber });

            foreach (var r in res2)
            {
                Console.WriteLine($"{r.StoreName} er i nærheden af {r.StreetName} {r.StreenNumber}.");
            }

            Console.WriteLine("------------------------");
            Console.WriteLine();

            var storesGroubedByZip = from store in stores
                                     group store by store.Zip;

            foreach (var groupBy in storesGroubedByZip)
            {
                Console.WriteLine($"Butikker i {groupBy.Key}");
                foreach (var store in groupBy)
                {
                    Console.WriteLine($"\t{store.Name}");
                }
            }

            Console.WriteLine("------------------------");
            Console.WriteLine();

            var storesGroupedByZipOrderedByZip = from store in stores
                                                 group store by store.Zip
                                                 into storesGrouped
                                                 orderby storesGrouped.Key
                                                 select storesGrouped;

            foreach (var groupBy in storesGroupedByZipOrderedByZip)
            {
                Console.WriteLine($"Butikker i {groupBy.Key}");
                foreach (var store in groupBy)
                {
                    Console.WriteLine($"\t{store.Name}");
                }
            }

            Console.WriteLine("------------------------");
            Console.WriteLine();

            var storesGrpByZip = stores
                .GroupBy(s => s.Zip)
                .OrderBy(g => g.Key)
                .Select(s => s);

            foreach (var groupBy in storesGrpByZip)
            {
                Console.WriteLine($"Butikker i {groupBy.Key}");
                foreach (var store in groupBy)
                {
                    Console.WriteLine($"\t{store.Name}");
                }
            }

            Console.WriteLine("------------------------");
            Console.WriteLine();

            var numberOfStoresByZip = from store in stores
                                      group store by store.Zip into storeGroup
                                      orderby storeGroup.Key
                                      select new { Zip = storeGroup.Key, Count = storeGroup.Count() };

            foreach (var group in numberOfStoresByZip)
            {
                Console.WriteLine($"I postnummer {group.Zip} er der {group.Count} butikker.");
            }

            Console.ReadLine();
        }
    }
}


