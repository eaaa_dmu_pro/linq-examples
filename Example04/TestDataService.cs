﻿using Example04.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example04
{
    public static class TestDataService
    {
        public static List<Address> GetAddresses()
        {
            return new List<Address>()
            {
                new Address()
                {
                    StreetName ="Sønderhøj", StreetNumber="30", Zip="8260"
                },
                new Address()
                {
                    StreetName ="Aabogade", StreetNumber="34", Zip="8200"
                },
                new Address()
                {
                    StreetName ="Park Allé", StreetNumber="2", Zip="8000"
                }
            };
        }

        public static List<Store> GetStores()
        {
            return new List<Store>()
            {
                new Store()
                {
                    Name = "Føtex Storcenter Nord", Zip="8200"
                },
                new Store()
                {
                    Name = "Føtex Viby Centeret", Zip="8260"
                },
                new Store()
                {
                    Name = "Kvickly Mega Syd", Zip="8260"
                },
                new Store()
                {
                    Name = "Føtex Frederiks Allé", Zip="8000"
                },
                new Store()
                {
                    Name = "Kvickly Bruuns Galleri", Zip="8000"
                },
            };
        }
            
    }
}
