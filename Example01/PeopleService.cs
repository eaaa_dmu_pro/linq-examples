﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example01
{
    public static class PeopleService
    {
        public static List<Person> GetTestData()
        {
            return new List<Person>
            {
                new Person { Name = "Michael", Rating = 8, Approved = true },
                new Person { Name = "Susan", Rating = 9, Approved = true },
                new Person { Name = "Ben", Rating = 5, Approved = false },
                new Person { Name = "Camilla", Rating = 7, Approved = true },
                new Person { Name = "Lars", Rating = 8, Approved = true }
            };
        }
    }
}
