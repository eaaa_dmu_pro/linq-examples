﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example01
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> people = PeopleService.GetTestData();
            Console.WriteLine("All persons:");
            people.ForEach(p => Console.WriteLine(p));
            Console.WriteLine();

            //Without Linq
            //
            //  List<Person> apprPersons = new List<Person>();
            //  foreach (var person in people)
            //  {
            //      if (person.Approved)
            //          apprPersons.Add(person);
            //  }
            IEnumerable<Person> apprPersons = people.Where(p => p.Approved);
            Console.WriteLine("Approved persons:");
            apprPersons.ToList().ForEach(p => Console.WriteLine(p));
            Console.WriteLine();

            IOrderedEnumerable<Person> apprPersonsOrdered = apprPersons.OrderBy(p => p.Rating);
            Console.WriteLine("Approved persons ordered:");
            apprPersonsOrdered.ToList().ForEach(p => Console.WriteLine(p));
            Console.WriteLine();

            IEnumerable<String> namesOfApprPersonsOrdered = apprPersonsOrdered.Select(p => p.Name);
            Console.WriteLine("Names of approved persons ordered:");
            namesOfApprPersonsOrdered.ToList().ForEach(name => Console.WriteLine(name));
            Console.WriteLine();

            IEnumerable<string> selected = people.Where(p => p.Approved).OrderBy(p => p.Rating).Select(p => p.Name);
            Console.WriteLine("Names of selected persons:");
            selected.ToList().ForEach(p => Console.WriteLine(p));
            Console.WriteLine();

            var selected1 = people.Where(p => p.Approved).OrderBy(p => p.Rating).Select(p => p.Name);
            Console.WriteLine("Names of selected with code in one line:");
            selected1.ToList().ForEach(p => Console.WriteLine(p));
            Console.WriteLine();

            var selected2 = from p in people
                            where p.Approved
                            orderby p.Rating
                            select p.Name;

            Console.WriteLine("Names of selected using SQL-like syntax:");
            selected2.ToList().ForEach(name => Console.WriteLine(name));
            Console.WriteLine();

            Console.WriteLine("-----------------------");

            int[] nums = { 20, 1, 2, 30, 3, 40, 8, 50 };
            var selnums = from e in nums
                          where e < 10
                          select e;

            // writes 1 2 3 8
            foreach (var n in selnums)
            {
                Console.Write(n + " ");
            }
            Console.WriteLine();

            nums[0] = 7;
            // writes 7 1 2 3 8
            foreach (var n in selnums)
            {
                Console.Write(n + " ");
            }
            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("All code in one line:");
            nums.Where(e => e < 10).Select(e => e).ToList().ForEach(e => Console.Write(e + " "));
            Console.WriteLine();

            Console.WriteLine("Using SQL-like syntax:");
            (from e in nums where e < 10 select e).ToList().ForEach(e => Console.Write(e + " "));
            Console.WriteLine();

            Console.ReadLine();
        }
    }
}

