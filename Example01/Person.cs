﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example01
{
    public class Person
    {
        public string Name { get; set; }
        public int Rating { get; set; }
        public bool Approved { get; set; }

        public override string ToString()
        {
            string app = Approved ? "T" : "F";
            return $"{Name} ({Rating}, {(Approved ? "Godkendt" : "Ikke Godkendt")})";
        }
    }

}
