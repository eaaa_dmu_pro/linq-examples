﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example02
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] colors = { "Light Red", "Green", "Yellow", "Dark Red", "Red", "Purple" };

            var reds2 = Array.FindAll(colors, x => x.Contains("Red"));
            Array.ForEach(reds2, c => Console.WriteLine(c));

            var withRed = from c in colors
                          where c.Contains("Red")
                          select c;

            string[] reds = withRed.ToArray();

            for (int i = 0; i < reds.Length; i++)
            {
                Console.WriteLine(reds[i]);
            }

            // or:
            //string[] reds = (from c in colors where c.Contains("Red") select c).ToArray();

            var n = (from c in colors where c.Contains("Red") select c).Count();
            Console.WriteLine("Count of reds:" + n);

            Console.ReadLine();
        }
    }

}
